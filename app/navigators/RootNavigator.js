import React, { Component } from 'react';
import { AppState, BackHandler, NetInfo, Dimensions, Alert } from 'react-native';
import { connect } from 'react-redux';
import { createStackNavigator, createDrawerNavigator } from 'react-navigation';
import { addNavigationHelpers, createBottomTabNavigator, TabNavigator, TabBarBottom } from 'react-navigation';

import { changeAppState, changeConnectionStatus } from '../stores/appState/actions';
import { ScreenOrientation } from 'expo';
import Game2048Container from '../containers/Game2048Container';
import SplashContainer from '../containers/SplashContainer';
import ReportNoShopContainer from '../containers/ReportNoShopContainer';
import RootNavigationService from '../services/navigation/RootNavigationService';
import DrawerMenu from '../components/DrawerMenu';

export const RootNavigator = createBottomTabNavigator({
  SplashContainer: { screen: SplashContainer },
  Game2048: { screen: Game2048Container },
  ReportNoShop: { screen: ReportNoShopContainer },
});

class RootWithNavigationState extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    ScreenOrientation.allow(ScreenOrientation.Orientation.PORTRAIT_UP);
  }

  componentDidMount() {
    NetInfo.isConnected.addEventListener('connectionChange', this.onConnectionChange);
    BackHandler.addEventListener('hardwareBackPress', this.onBackPressed);
    AppState.addEventListener('change', this.onAppStateChange);
    Dimensions.addEventListener('change', this.onDimensionChange);
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener('connectionChange', this.onConnectionChange);
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed);
    AppState.removeEventListener('change', this.onAppStateChange);
    Dimensions.removeEventListener('change', this.onDimensionChange);
  }
  onBackPressed = () => {
    return false;
  };

  onAppStateChange = nextAppState => {
    if (this.props.appState.app.state) {
      if (this.props.appState.app.state.match(/inactive|background/) && nextAppState === 'active') {
        // app resume
        ServerTime.onAppResume();
        firebaseHelper.tracking().trackEvent(TRACK_EVENT.APP_STATE_CHANGE, 'resume');
      } else if (this.props.appState.app.state === 'active' && nextAppState.match(/inactive|background/)) {
        // app pause
        ServerTime.onAppPause();
        firebaseHelper.tracking().trackEvent(TRACK_EVENT.APP_STATE_CHANGE, 'pause');
      }
    }
    this.props.dispatch(changeAppState(nextAppState));
  };

  onConnectionChange = isConnected => {
    this.props.dispatch(changeConnectionStatus(isConnected));
    if (!isConnected) {
      this.showAlertConnect();
    }
  };

  showAlertConnect() {
    Alert.alert(
      '',
      `Ứng dụng bị mất kết nối, vui lòng kiểm tra lại kết nối Internet`,
      [
        {
          text: 'Thử lại',
          onPress: () => {
            !this.props.appState.network.isConnected ? this.showAlertConnect() : null;
          },
        },
      ],
      { cancelable: false }
    );
  }

  render() {
    return (
      <RootNavigator
        ref={navigatorRef => {
          RootNavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}

const mapStateToProps = state => ({
  appState: state.appState,
});

export default connect(mapStateToProps)(RootWithNavigationState);
