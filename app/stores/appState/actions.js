import * as types from './action-types';
// import config from '../../config/testclound-key.json'
export function changeConnectionStatus(status) {
  return {
    type: types.CHANGE_CONNECTION_STATUS,
    isConnected: status,
  };
}

export function changeAppState(key, storeOrder, callback) {
  return async (dispatch, getState) => {
    try {
      const firebase = require('firebase');
      // Required for side-effects
      require('firebase/firestore');
      let config = {
        apiKey: 'AIzaSyCSEP3nV4uTdySu72OMvsnNr8XFZjXcJ5Q',
        authDomain: 'test-clound.firebaseapp.com',
        databaseURL: 'https://test-clound.firebaseio.com',
        projectId: 'test-clound',
        storageBucket: 'test-clound.appspot.com',
        messagingSenderId: '1058491429049',
      };
      if (firebase.apps.length === 0) {
        firebase.initializeApp(config);
      }

      // firebase.initializeApp(config);
      let db = firebase.firestore();
      db.settings({ timestampsInSnapshots: true });
      await db
        .collection('users')
        .get()
        .then(function(querySnapshot) {
          querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, ' => ', doc.data());
          });
        });
    } catch (error) {}
  };
}

export function showHideLoadingModal(message, visible, onConfirm) {
  return {
    type: types.SHOW_HIDE_LOADING_MODAL,
    message,
    visible,
    onConfirm,
  };
}
