import { combineReducers } from 'redux';

import appState from './appState/reducer';
import * as utils from '../utils/index';

import * as appStateActions from './appState/action-types';

// Combines all reducers to a single reducer function
const reducers = combineReducers({
  appState: utils.ignoreReducer(appState, appStateActions)
});

export default reducers;
