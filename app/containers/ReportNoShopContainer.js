import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import * as utils from '../utils';
import * as AppStateActions from '../stores/appState/actions';
import { NavigationActions } from 'react-navigation';

export class ReportNoShopContainer extends Component {
  static navigationOptions = ({ navigation, screenProps }) => ({
    tabBarLabel: '1235',
    tabBarVisible:
      navigation.state.params && navigation.state.params.tabBarVisible !== undefined
        ? navigation.state.params.tabBarVisible
        : true,
    tabBarIcon: ({ focused, tintColor }) => (
      <View accessibilityLabel="cs_navigator">
        {/* <IconSAT name="Gcafe" size={30} color={focused ? '#008764' : '#696969'} /> */}
      </View>
    ),
    tabBarOnPress: ({ previousScene, scene, jumpTo }) => {
      if (navigation.state.params && navigation.state.params.thisPtr) {
        navigation.state.params.thisPtr.onTabIconPressed(scene, jumpToIndex);
      } else {
        jumpTo(scene.index);
      }
    },
  });

  state = {
    time: 0,
    date: 0,
  };

  // demoLog = () => {
  //   setInterval(() => {
  //     this.setState({
  //       time: this.state.time + 1
  //     });
  //   }, 500);
  // };
  // demoDate = () => {
  //   setInterval(() => {
  //     this.setState({
  //       date: this.state.date + 1
  //     });
  //   }, 1000);
  // };

  onResult() {}

  render() {
    // console.log('222', this.demoDate(), this.demoDate);
    console.log('1234', this.state.time, this.state.date);
    return (
      <View style={{ flex: 1, flexDirection: 'column', backgroundColor: '#e5e5e5' }}>
        <View
          style={{
            height: 50,
            backgroundColor: '#1c65aa',
          }}
        />
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: 15,
              textAlign: 'center',
            }}
          >
            Tài khoản của bạn chưa được
            {'\n'} gán vào cửa hàng nào
            {'\n'}
          </Text>
          <TouchableOpacity
            style={{ justifyContent: 'flex-start', alignItems: 'center' }}
            onPress={
              // () => {
              //   this.demoLog();
              //   this.demoDate();
              // }

              () => {
                this.props.changeAppState(1);
              }
            }
          >
            <Text
              style={{
                borderRadius: 10,
                overflow: 'hidden',
                backgroundColor: '#1c65aa',
                padding: 10,
                margin: 10,
                fontWeight: 'bold',
                fontSize: 15,
                color: 'white',
              }}
            >
              Đăng xuất 1
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    changeAppState: state => dispatch(AppStateActions.changeAppState(state)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReportNoShopContainer);

const styles = StyleSheet.create({
  inputContainer: {
    alignItems: 'center',
    padding: 15,
    flexDirection: 'row',
  },
  headerContainer: {
    backgroundColor: '#1c65aa',
    paddingTop: 10,
  },
});
