import React, { Component } from 'react';
import { Text, View, StyleSheet, Dimensions, Alert } from 'react-native';
import { connect } from 'react-redux';
import * as utils from '../utils';
import * as AppStateActions from '../stores/appState/actions';
const { width, height } = Dimensions.get('window');
const change = width / 4;

export class Game2048Container extends Component {
  render() {
    return <View style={{ backgroundColor: 'red' }} />;
  }
}

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    changeAppState: state => dispatch(AppStateActions.changeAppState(state)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Game2048Container);

const styles = StyleSheet.create({
  inputContainer: {
    alignItems: 'center',
    padding: 15,
    flexDirection: 'row',
  },
  headerContainer: {
    backgroundColor: '#1c65aa',
    paddingTop: 10,
  },
});
