import React, { Component } from 'react';
import { Image, Dimensions, Text, Platform, ActivityIndicator, StyleSheet, View, Alert } from 'react-native';
import { Asset, Font, AppLoading } from 'expo';
import { connect } from 'react-redux';
import { screen } from '../resources/styles/common';
import RootNavigationService from '../services/navigation/RootNavigationService';
import { scale } from '../utils/scaling';
import Logger from '../utils/logger';
import config from '../config';

export class SplashContainer extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = { loading: true };
    this.onLoadSuccess = this.onLoadSuccess.bind(this);
  }

  async loadResources() {
    Logger.startMeasure('init');

    return Promise.all([
      Font.loadAsync({
        saletool: require('../resources/fonts/saletool.ttf'),
        'sale-text-italic': Platform.select({
          ios: require('../resources/fonts/ios/SF-UI-Text-Italic.ttf'),
          android: require('../resources/fonts/android/Roboto-Italic.ttf'),
        }),
        'sale-text-light': Platform.select({
          ios: require('../resources/fonts/ios/SF-UI-Text-Light.ttf'),
          android: require('../resources/fonts/android/Roboto-Light.ttf'),
        }),
        'sale-text-light-italic': Platform.select({
          ios: require('../resources/fonts/ios/SF-UI-Text-LightItalic.ttf'),
          android: require('../resources/fonts/android/Roboto-LightItalic.ttf'),
        }),
        'sale-text-medium': Platform.select({
          ios: require('../resources/fonts/ios/SF-UI-Text-Medium.ttf'),
          android: require('../resources/fonts/android/Roboto-Medium.ttf'),
        }),
        'sale-text-regular': Platform.select({
          ios: require('../resources/fonts/ios/SF-UI-Text-Regular.ttf'),
          android: require('../resources/fonts/android/Roboto-Regular.ttf'),
        }),
        'sale-text-bold': Platform.select({
          ios: require('../resources/fonts/ios/SF-UI-Text-Bold.ttf'),
          android: require('../resources/fonts/android/Roboto-Bold.ttf'),
        }),
        'sale-text-semibold': Platform.select({
          ios: require('../resources/fonts/ios/SF-UI-Text-Semibold.ttf'),
          android: require('../resources/fonts/android/Roboto-Medium.ttf'),
        }),
      }),
      Asset.loadAsync([require('../resources/images/no_product.png'), require('../resources/images/logo-top.png')]),
    ]);
  }

  onLoadSuccess() {
    this.setState({ loading: false });
    this.props.navigation.navigate('Game2048');
  }

  render() {
    return this.state.loading ? (
      <AppLoading startAsync={this.loadResources} onFinish={this.onLoadSuccess} onError={console.warn} />
    ) : (
      <View style={styles.container}>
        <View style={styles.mainContent}>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} />
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    appVersion: config.version,
    loadingStep: state.appState.loadingStep,
  };
}

export default connect(mapStateToProps)(SplashContainer);

const styles = StyleSheet.create({});
