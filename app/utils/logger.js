import config from '../config';
// import firebaseHelper from '../services/firebase';
// import { TRACK_EVENT } from '../config/TrackingConstants';

class Logger {
  constructor() {
    this.measures = {};
  }

  write(...args) {
    if (config.log_enabled) {
      console.log(...args);
    }
  }

  startMeasure(label) {
    this.measures[label] = {
      latest: Date.now(),
      isEnd: false,
      phases: [],
      category: {},
    };
  }

  addMeasurePhase(label, phase) {
    let now = Date.now();
    let m = this.measures[label];

    if (m && !m.isEnd) {
      m.phases.push({
        phase,
        ellapsed: now - m.latest,
      });
      m.latest = now;
    }
  }

  endMeasure(label, isExcept = false, print = false) {
    if (this.measures[label] && !this.measures[label].isEnd) {
      this.addMeasurePhase(label, isExcept ? 'except' : 'end');
      this.measures[label].isEnd = true;

      let data = this.getMeasureData(label);
      print && this.printMeasure(label, data);
      // firebaseHelper.tracking().trackEvent(TRACK_EVENT.MEASUREMENT, data.total, label, data.phases);
    }
  }

  clearMeasure(label) {
    if (this.measures[label]) {
      delete this.measures[label];
    }
  }

  getMeasureData(label) {
    if (this.measures[label]) {
      return {
        label,
        total: this.measures[label].phases.reduce((acc, val) => acc + val.ellapsed, 0),
        phases: this.measures[label].phases,
      };
    }
  }

  printMeasure(label, data = null) {
    if (config.profiler_enabled) {
      let d = data ? data : this.getMeasureData(label);
      if (d) {
        console.log(`MEASURE:`, d);
      } else {
        console.log(`NO MEASUREMENT FOR ${label}`);
      }
    }
  }
}

export default new Logger();
