import { ignoreActions } from 'redux-ignore';

export function checkActionIgnore(type, types) {
  for (let key of Object.keys(types)) {
    if (types[key] === type) {
      return true;
    }
  }
  return false;
}

export function ignoreReducer(reducer, actionTypes) {
  return ignoreActions(reducer, action => {
    checkActionIgnore(action.type, actionTypes);
  });
}
