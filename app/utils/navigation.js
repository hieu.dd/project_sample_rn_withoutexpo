import { NavigationActions } from 'react-navigation';

// reference: https://reactnavigation.org/docs/en/navigating-without-navigation-prop.html
class NavigationService {
  navigator = null;

  setTopLevelNavigator(navigatorRef) {
    this.navigator = navigatorRef;
  }

  navigate(routeName, params) {
    this.navigator &&
      this.navigator.dispatch(
        NavigationActions.navigate({
          routeName,
          params,
        })
      );
  }

  dispatch(action) {
    this.navigator && this.navigator.dispatch(action);
  }
}

export function createNavigationService() {
  return new NavigationService();
}

export function getActiveRouteName(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  if (route.routes) {
    return getActiveRouteName(route);
  }

  return route.routeName;
}
