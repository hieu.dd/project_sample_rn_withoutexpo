var MAPPING = {
  cidx: 0,
  category: {},
  iidx: 0,
  inventory: {}
};

var INVERT_MAPPING = {
  category: {},
  inventory: {}
};

const MINIFY_TEMPLATE = {
  KEY_MAP: {
    sku: 's',
    name: 'n',
    status: 'u',
    price_w_vat: 'p',
    category: 'c',
    original_price: 'o',
    inventories: 'i',
    promotions: 'm',
    keywords: 'k',
    gift: 'g',
    notes: 't',
    leaflets: 'l',
    membership: 'h',
    sale_price: 'r',
    grand_total: 'd'
  },
  INVERT_KEY_MAP: {
    s: 'sku',
    n: 'name',
    u: 'status',
    p: 'price_w_vat',
    c: 'category',
    o: 'original_price',
    i: 'inventories',
    m: 'promotions',
    k: 'keywords',
    g: 'gift',
    t: 'notes',
    l: 'leaflets',
    h: 'membership',
    r: 'sale_price',
    d: 'grand_total'
  },
  TRANFORM: {
    price_w_vat: { min: divide1000, exp: multiply1000 },
    original_price: { min: divide1000, exp: multiply1000 },
    category: { min: mapCategory, exp: invertMapCategory },
    inventories: { min: mapInventories, exp: invertMapInventories }
    // promotions: { min: minSubKey, exp: expSubKey }
  }
};

// MAP function
function divide1000(v) {
  return parseInt(v / 1000);
}

function mapCategory(c) {
  if (!c) {
    return c;
  }

  if (!MAPPING.category[c]) {
    MAPPING.cidx += 1;
    MAPPING.category[c] = MAPPING.cidx;
    INVERT_MAPPING.category[MAPPING.cidx] = c;
  }

  return MAPPING.category[c];
}

function mapInventories(i) {
  if (!i) {
    return i;
  }

  let ret = {};
  for (let stock_id in i) {
    if (!MAPPING.inventory[stock_id]) {
      MAPPING.iidx += 1;
      MAPPING.inventory[stock_id] = MAPPING.iidx;
      INVERT_MAPPING.inventory[MAPPING.iidx] = stock_id;
    }
    ret[MAPPING.inventory[stock_id]] = i[stock_id];
  }

  return ret;
}

// INVERT MAP function
function multiply1000(v) {
  return v * 1000;
}

function invertMapCategory(c) {
  return INVERT_MAPPING.category[c] || c;
}

function invertMapInventories(i) {
  let ret = {};
  for (let stock_id in i) {
    let key = INVERT_MAPPING.inventory[stock_id] || stock_id;
    ret[key] = i[stock_id];
  }

  return ret;
}

function minSubKey(p) {
  const { KEY_MAP } = MINIFY_TEMPLATE;
  let ret = {};
  for (let key in p) {
    let tkey = KEY_MAP[key] || key;
    ret[tkey] = p[key];
  }
  return ret;
}

function expSubKey(p) {
  const { INVERT_KEY_MAP } = MINIFY_TEMPLATE;
  let ret = {};
  for (let key in p) {
    let tkey = INVERT_KEY_MAP[key] || key;
    ret[tkey] = p[key];
  }
  return ret;
}

module.exports = {
  minifyProducts: function(products) {
    const { KEY_MAP, TRANFORM } = MINIFY_TEMPLATE;
    MAPPING = {
      cidx: 0,
      category: {},
      iidx: 0,
      inventory: {}
    };

    let data = products.map(p => {
      let ret = {};
      for (let key in p) {
        let tkey = KEY_MAP[key] || key;
        ret[tkey] = TRANFORM[key] ? TRANFORM[key].min(p[key]) : p[key];
      }
      return ret;
    });

    return { products: data, mapping: INVERT_MAPPING };
  },

  expandProducts: function(products, invertMapping) {
    const { INVERT_KEY_MAP, TRANFORM } = MINIFY_TEMPLATE;
    INVERT_MAPPING = invertMapping;

    return products.map(p => {
      let ret = {};
      for (let key in p) {
        let tkey = INVERT_KEY_MAP[key] || key;
        ret[tkey] = TRANFORM[tkey] ? TRANFORM[tkey].exp(p[key]) : p[key];
      }
      return ret;
    });
  },

  minifyCommunes: function(communes) {
    let compress = {};

    for (let c of communes) {
      let { name, id, district_id } = c;
      id = id.replace(district_id, '');
      if (name.charAt(0) === 'X') {
        name = name.replace('Xã ', '%');
        name = name.replace('Xă ', '%');
      } else if (name.charAt(0) === 'P') {
        name = name.replace('Phường ', '$');
      } else if (name.charAt(0) === 'T') {
        name = name.replace('Thị trấn ', '@');
        name = name.replace('Thị Trấn ', '@');
      }

      compress[c.district_id] = { ...compress[c.district_id], [id]: name };
    }

    return compress;
  },

  expandCommunes: function(communes) {
    let ret = {};

    for (let district_id in communes) {
      let ids = Object.keys(communes[district_id]);
      ret[district_id] = ids.map(i => {
        let name = communes[district_id][i];
        if (name.charAt(0) === '%') {
          name = name.replace('%', 'Xã ');
        } else if (name.charAt(0) === '$') {
          name = name.replace('$', 'Phường ');
        } else if (name.charAt(0) === '@') {
          name = name.replace('@', 'Thị Trấn ');
        }

        return {
          key: `${district_id}${i}`,
          label: name
        };
      });

      ret[district_id].sort((a, b) => a.label.localeCompare(b.label, 'vi'));
    }

    return ret;
  },

  minifyDistricts: function(districts) {
    let compress = {};

    for (let d of districts) {
      let { name, id, province_id } = d;
      id = id.replace(province_id, '');
      if (name.charAt(0) === 'Q') {
        name = name.replace('Quận ', '%');
      } else if (name.charAt(0) === 'H') {
        name = name.replace('Huyện ', '$');
      } else if (name.charAt(0) === 'T') {
        name = name.replace('Thị xã ', '@');
        name = name.replace('Thành phố ', '#');
      }

      compress[d.province_id] = { ...compress[d.province_id], [id]: name };
    }

    return compress;
  },

  expandDistricts: function(districts) {
    let ret = {};

    for (let province_id in districts) {
      let ids = Object.keys(districts[province_id]);
      ret[province_id] = ids.map(i => {
        let name = districts[province_id][i];
        if (name.charAt(0) === '%') {
          name = name.replace('%', 'Quận ');
        } else if (name.charAt(0) === '$') {
          name = name.replace('$', 'Huyện ');
        } else if (name.charAt(0) === '@') {
          name = name.replace('@', 'Thị xã ');
        } else if (name.charAt(0) === '#') {
          name = name.replace('#', 'Thành phố ');
        }

        return {
          key: `${province_id}${i}`,
          label: name
        };
      });

      ret[province_id].sort((a, b) => a.label.localeCompare(b.label, 'vi'));
    }

    return ret;
  }
};
