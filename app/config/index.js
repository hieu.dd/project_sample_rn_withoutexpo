module.exports = {
  env: 'dev',
  version: '1.0.9.4019',
  store_version: '1.0.9',
  log_enabled: true,
  profiler_enabled: false,
  hosts: {
    sso: 'acc.teko.vn',
    offlinesales: 'test.offlinesales.teksvr.com',
    magento: 'dev.tekshop.vn',
    asia: 'dev.pvis.teko.vn',
    crm: 'dev-crm.teko.vn',
    notification: 'test.stn.teksvr.com',
    om: 'om-dev.teko.vn'
  },
  etag: {
    json_version: 24,
    value: 'ea60fcda7e15cc0fe64f7f5e8f672861',
    datetime: '2018-06-13T15:00:00.000000+07:00'
  },
  firebase: {
    apiKey: 'AIzaSyA_81aCNGIKN5EqjderFnm3vUhl7yZY4vg',
    authDomain: 'nhan-vien-phong-vu-dev.firebaseapp.com',
    databaseURL: 'https://nhan-vien-phong-vu-dev.firebaseio.com',
    projectId: 'nhan-vien-phong-vu-dev',
    storageBucket: 'nhan-vien-phong-vu-dev.appspot.com'
  },
  AMPLITUDE_API_KEY: 'a6d4069c40b2e875cbf26807bea87c80'
}