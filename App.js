import React from "react";
import { UIManager } from "react-native";
import { Provider } from "react-redux";
import { StyleSheet, Text, View, StatusBar } from "react-native";
import RootWithNavigationState from "./app/navigators/RootNavigator";
import store from "./app/stores/configureStore";
// import ProcessingPrompt from './app/components/ProcessingPrompt';
// import { MenuProvider } from 'react-native-popup-menu';

export default class App extends React.Component {
  componentWillMount() {
    UIManager.setLayoutAnimationEnabledExperimental &&
      UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  render() {
    return (
      <Provider store={store}>
        <View style={{ flex: 1 }}>
          {/* <MenuProvider> */}
          <StatusBar backgroundColor="transparent" barStyle="light-content" />
          {/* <RootWithNavigationState /> */}
          {/* <ProcessingPrompt /> */}
          {/* </MenuProvider> */}
        </View>
      </Provider>
    );
  }
}
