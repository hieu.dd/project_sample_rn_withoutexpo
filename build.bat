@echo off
set PLATFORM=%1
set CHANNEL=dev

if "%PLATFORM%"=="ios" (
    echo exp build:ios --release-channel %CHANNEL%
    call exp build:ios --release-channel %CHANNEL%
    goto end
)
if "%PLATFORM%"=="android" (
    echo exp build:android --release-channel %CHANNEL%
    call exp build:android --release-channel %CHANNEL%
    goto end
)
if "%PLATFORM%"=="" (
    echo exp publish --release-channel %CHANNEL%
    call exp publish --release-channel %CHANNEL%
    goto end
)

:invalid_platform
echo Invalid platforms! available platforms are `ios` and `android`. Call with empty platform to publish only.

:end
