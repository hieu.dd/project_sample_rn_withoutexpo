var fs = require('fs');

var COMMANDS = {};

var LOGSTREAM = fs.createWriteStream('log.txt');
LOGSTREAM.once('open', function(fd) {
  main();
});

async function main() {
  initCommands();

  for (let func in COMMANDS) {
    let i = process.argv.findIndex(a => a === `--${func}`);
    if (i !== -1) {
      await COMMANDS[func]();
      break;
    }
  }

  LOGSTREAM.end();

  console.log('Done!!!');
  console.log('Press Ctrl+C to finish task');

  return false;
}

function initCommands() {
  registerCommand('measure', summarizeMeasurement);
}

function registerCommand(name, func) {
  COMMANDS[name] = func;
}

// FUNCTION COMMANDS START FROM HERE
async function summarizeMeasurement() {
  // Description:
  let data = require('./data/nhan-vien-phong-vu-measurement-export.json');
  let sum = {};

  for (let key in data) {
    let entry = data[key];

    if (!sum[entry.name]) {
      sum[entry.name] = {
        count: 0,
        average: 0,
        max: 0,
        min: 999999999,
        phases: {}
      };
    }

    let total = 0;
    for (let phase of entry.data) {
      if (!sum[entry.name].phases[phase.phase]) {
        sum[entry.name].phases[phase.phase] = {
          count: 0,
          average: 0,
          max: 0,
          min: 999999999
        };
      }

      let currentPhase = sum[entry.name].phases[phase.phase];
      currentPhase.count += 1;
      currentPhase.average += phase.ellapsed;
      currentPhase.max = phase.ellapsed > currentPhase.max ? phase.ellapsed : currentPhase.max;
      currentPhase.min = phase.ellapsed < currentPhase.min ? phase.ellapsed : currentPhase.min;
      total += phase.ellapsed;
    }

    sum[entry.name].count += 1;
    sum[entry.name].average += total;
    sum[entry.name].max = total > sum[entry.name].max ? total : sum[entry.name].max;
    sum[entry.name].min = total < sum[entry.name].min ? total : sum[entry.name].min;
  }

  for (let measure in sum) {
    sum[measure].average = Math.floor(sum[measure].average / sum[measure].count);
    for (let phase in sum[measure].phases) {
      sum[measure].phases[phase].average = Math.floor(sum[measure].phases[phase].average / sum[measure].phases[phase].count);
    }
  }

  await fs.writeFile('output.json', JSON.stringify(sum, null, 2));
}
