var fs = require('fs');
var LZString = require('lz-string');
var DataOptimizer = require('../../app/utils/optimizer');

var COMMUNES = require('./data/communes.json');
var DISTRICTS = require('./data/districts.json');
var PRODUCT_SPECS = require('./data/product_specification.json');
var PRODUCTS = [];
var ENV = 'dev';
var VERSION = 1;
var ETAG = 'not available';

function run(env = 'dev', version = 2) {
  ENV = env;
  if (!isNaN(version)) VERSION = parseInt(version);
  else console.log('Version must be an integer');
  loadData(1);
  //minifyData();
  //deminifyData();
}

NOW = null;

function loadData(i) {
  NOW = Date.now();
  fs.readFile(`${__dirname}/data/${ENV}/product_${i}.json`, (err, p) => {
    if (!err) {
      if (VERSION === 1) {
        console.log(`Merging from ${__dirname}/data/${ENV}/product_${i}.json with Version = ${VERSION}`);
        let data = JSON.parse(p);
        mergeData(data);
      } else if (VERSION === 2) {
        let data = JSON.parse(p.toString().replace(/\]\[/g, ','));
        for (let entry of data) {
          ETAG = entry.new_etag;
          mergeData(entry);
        }
      }
      loadData(i + 1);
    } else {
      writeData();
    }
  });
}

function writeData() {
  data = DataOptimizer.minifyProducts(PRODUCTS);
  fs.writeFile(`${__dirname}/../../app/config/products.json`, JSON.stringify(data.products), function(err) {
    if (err) {
      console.log('writeFile: ', err);
    } else {
      console.log(`SUCCESS write products.json! ETAG ${ETAG}`);
      console.log(`Runtime: ${Date.now() - NOW} ms`);
    }
  });
  fs.writeFile(`${__dirname}/../../app/config/mapping.json`, JSON.stringify(data.mapping), function(err) {
    if (err) {
      console.log('writeFile: ', err);
    } else {
      console.log(`SUCCESS write mapping.json!`);
      console.log(`Runtime: ${Date.now() - NOW} ms`);
    }
  });

  COMMUNES = DataOptimizer.minifyCommunes(COMMUNES);
  fs.writeFile(`${__dirname}/../../app/config/communes.json`, JSON.stringify(COMMUNES), function(err) {
    if (err) {
      console.log('writeFile: ', err);
    } else {
      console.log(`SUCCESS write communes.json!`);
      console.log(`Runtime: ${Date.now() - NOW} ms`);
    }
  });

  DISTRICTS = DataOptimizer.minifyDistricts(DISTRICTS);
  fs.writeFile(`${__dirname}/../../app/config/districts.json`, JSON.stringify(DISTRICTS), function(err) {
    if (err) {
      console.log('writeFile: ', err);
    } else {
      console.log(`SUCCESS write districts.json!`);
      console.log(`Runtime: ${Date.now() - NOW} ms`);
    }
  });

  PRODUCT_SPECS = PRODUCT_SPECS.reduce((acc, cur) => {
    acc[cur.attribute_code] = cur.frontend_label;
    return acc;
  }, {});
  fs.writeFile(`${__dirname}/../../app/config/product_specification.json`, JSON.stringify(PRODUCT_SPECS), function(err) {
    if (err) {
      console.log('writeFile: ', err);
    } else {
      console.log(`SUCCESS write product_specification.json!`);
      console.log(`Runtime: ${Date.now() - NOW} ms`);
    }
  });
}

function mergeData(data) {
  let products = data.results
    .map(p => {
      if (
        isNaN(parseInt(p.sku)) ||
        p.sku.length !== 7 ||
        !p.status ||
        (parseInt(p.status) < 1 || parseInt(p.status) > 6) ||
        !p.name
      ) {
        return null;
      }
      let inventories = p.inventories.reduce((acc, current) => {
        acc[current.store] = current.quantity;
        return acc;
      }, {});

      let product = {
        sku: p.sku,
        name: p.name,
        status: p.status,
        price_w_vat: parseInt(p.price_asia),
        category: p.asia_category
        //description: p.description,
        //warranty: p.warranty,
      };
      if (p.price_original && p.price_asia && parseInt(p.price_original) !== parseInt(p.price_asia)) {
        product.original_price = parseInt(p.price_original);
      }
      // if (p.price_gamenet && parseInt(p.price_gamenet) > 0) {
      //   product.price_gamenet = parseInt(p.price_gamenet);
      // }
      if (Object.keys(inventories).length > 0) {
        product.inventories = inventories;
      }
      if (
        p.promotions &&
        ((p.promotions.gift && p.promotions.gift.length > 0) ||
          (p.promotions.leaflets && p.promotions.leaflets.length > 0) ||
          (p.promotions.membership && p.promotions.membership.length > 0) ||
          (p.promotions.sale_price && p.promotions.sale_price.length > 0) ||
          (p.promotions.grand_total && p.promotions.grand_total.length > 0))
      ) {
        product.promotions = p.promotions;
      }
      return product;
    })
    .filter(p => p !== null);

  PRODUCTS = [...PRODUCTS, ...products];
}

function minifyData() {
  fs.readFile(`${__dirname}/../../app/config/products.json`, (err, p) => {
    if (!err) {
      let data = JSON.parse(p);
      let ret = {};

      for (let product of data) {
        if (product.sku.length === 7) {
          let before = parseInt(parseInt(product.sku) / 10);
          let after = parseInt(product.sku) - before * 10;

          ret[before] = ret[before] || {};
          ret[before][after] =
            product.price_gamenet > 0 ? parseInt(product.price_gamenet / 1000) : parseInt(product.price / 1000);
        }
      }

      let refine = JSON.stringify(ret).replace(/\"/g, '');

      let strCpr = LZString.compressToBase64(refine);
      fs.writeFile(`${__dirname}/compress.txt`, strCpr, function(err) {
        if (err) {
          console.log('minifyData: ', err);
        } else {
          console.log('SUCCESS!');
        }
      });
    } else {
      console.log('readFile: ', err);
    }
  });
}

function deminifyData() {
  fs.readFile(`${__dirname}/compress.txt`, (err, p) => {
    if (!err) {
      let refine = LZString.decompressFromBase64(p.toString());

      fs.writeFile(`${__dirname}/raw.txt`, refine, function(err) {
        if (err) {
          console.log('deminifyData: ', err);
        } else {
          console.log('SUCCESS!');
        }
      });
    } else {
      console.log('readFile: ', err);
    }
  });
}

module.exports = run;
