var fs = require('fs');
var LZString = require('lz-string');
var fetch = require('node-fetch');

var COMPARE_WITH_SERVER = false;
var ETAG = {};

var COMMANDS = {};

var LOGSTREAM = fs.createWriteStream('log.txt');
LOGSTREAM.once('open', function(fd) {
  main();
});

async function main() {
  initCommands();

  for (let func in COMMANDS) {
    let i = process.argv.findIndex(a => a === `--${func}`);
    if (i !== -1) {
      await COMMANDS[func]();
      break;
    }
  }

  LOGSTREAM.end();

  console.log('Done!!!');
  console.log('Press Ctrl+C to finish task');

  return false;
}

function initCommands() {
  registerCommand('decompress', decompress);
  registerCommand('trace', trace);
}

function registerCommand(name, func) {
  COMMANDS[name] = func;
}

// FUNCTION COMMANDS START FROM HERE
async function decompress() {
  let key = '-LL0IlQWMdaeu71avwqK';
  let INPUT = require('../firebase/snapshot_events_update_etag_user_id_pbbzQuY71sWrJ91ICwbpfaMDvlG3.json');
  let decoded = decompressJson(INPUT[key].data);

  await fs.writeFile(`./decoded_${INPUT[key].new_etag.value}`, JSON.stringify(decoded, null, 2));
}

async function trace() {
  let INPUT = require('./snapshot_events_update_etag_user_id_VVavl5taaFWGjvv4CZ0XbXJDznZ2.json');

  for (let key in INPUT) {
    let e = INPUT[key].new_etag.value;

    if (ETAG[e]) {
      ETAG[e].push({
        key,
        data: INPUT[key].data
      });
    } else {
      ETAG[e] = [
        {
          key,
          data: INPUT[key].data
        }
      ];
    }
  }

  for (let etag in ETAG) {
    try {
      if (COMPARE_WITH_SERVER) {
        let res = await fetch(`http://qc.offlinesales.teksvr.com/api/v2.1/etags/${etag}/`);
        let json = await res.json();

        if (json.changes_price) {
          for (let sku in json.changes_price) {
            let prices = json.changes_price[sku].split(';');
            let price_asia = parseInt(prices[0]);
            let price_original = parseInt(prices[1]);

            for (let clientData of ETAG[etag]) {
              let decoded = decompressJson(clientData.data);

              if (sku.length === 7) {
                let before = parseInt(parseInt(sku) / 10);
                let after = parseInt(sku) - before * 10;

                if (decoded[before][after] !== price_asia) {
                  console.log('mismatch');
                }
              }
            }
          }
        }
      } else {
        // check particular SKU
        let sku = '1800900';
        for (let clientData of ETAG[etag]) {
          let decoded = decompressJson(clientData.data);
          let before = parseInt(parseInt(sku) / 10);
          let after = parseInt(sku) - before * 10;

          console.log(`ETAG: ${etag}\nDATA: ${decoded[before][after]}`);
        }
      }
    } catch (error) {
      console.log(`Error when processing ${etag}: ${error}`);
    }
  }
}

function decompressJson(data) {
  let decoded = LZString.decompressFromBase64(data);
  decoded = decoded.replace(/(['"])?([a-z0-9A-Z_]+)(['"])?:/g, '"$2": ');
  return JSON.parse(decoded);
}
