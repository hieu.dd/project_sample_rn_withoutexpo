var fs = require('fs');
var firebase = require('firebase');
var config = require('../../app/config');

var COMMANDS = {};

var LOGSTREAM = fs.createWriteStream('log.txt');
LOGSTREAM.once('open', function(fd) {
  main();
});

async function main() {
  initCommands();
  firebase.initializeApp(config.firebase);

  for (let func in COMMANDS) {
    let i = process.argv.findIndex(a => a === `--${func}`);
    if (i !== -1) {
      await COMMANDS[func]();
      break;
    }
  }

  LOGSTREAM.end();

  console.log('Done!!!');
  console.log('Press Ctrl+C to finish task');

  return false;
}

function initCommands() {
  registerCommand('migrate', migrate_08012018);
}

function registerCommand(name, func) {
  COMMANDS[name] = func;
}

// FUNCTION COMMANDS START FROM HERE
async function migrate_08012018() {
  // Description:
  // Change shop key from firebase generated string to more readable one (CPXX, CHXXXX)
  // migration script go here
}
