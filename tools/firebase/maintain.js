var fs = require('fs');
var firebase = require('firebase');
var config = require('../../app/config');

var COMMANDS = {};

var LOGSTREAM = fs.createWriteStream('log.txt');
LOGSTREAM.once('open', function(fd) {
  main();
});

async function main() {
  initCommands();
  firebase.initializeApp(config.firebase);

  for (let func in COMMANDS) {
    let i = process.argv.findIndex(a => a === `--${func}`);
    if (i !== -1) {
      await COMMANDS[func]();
      break;
    }
  }

  LOGSTREAM.end();

  console.log('Done!!!');
  console.log('Press Ctrl+C to finish task');

  return false;
}

function initCommands() {
  registerCommand('removeExpired', removeExpiredPromotions);
}

function registerCommand(name, func) {
  COMMANDS[name] = func;
}

// FUNCTION COMMANDS START FROM HERE
async function removeExpiredPromotions() {
  let updates = {};
  let promotionsRef = firebase.database().ref('promotions-v2/promotions/');
  let currentDate = new Date().getTime();
  let promotions = [];

  await promotionsRef.once('value', function(snapshot) {
    promotions = snapshot.val().filter(item => item.date.endDate > currentDate);
  });

  // thêm dữ liệu tối thiểu nếu tất cả khuyến mãi promotions quá hạn
  if (promotions.length === 0) promotions = snapshot.val()[0];
  updates[`promotions-v2/promotions/`] = promotions;

  let promotionsNewRef = firebase.database().ref('promotions-v2/promotions_new/');
  let promotions_new = {};
  await promotionsNewRef.once('value', function(snapshot) {
    let production = snapshot.val();
    let keys = Object.keys(production);
    keys.map(key => {
      let promotion = production[key];
      if (promotion.dateTime.date.end > currentDate) {
        promotions_new[key] = promotion;
      }
    });

    // thêm dữ liệu tối thiểu nếu tất cả khuyến mãi promotions_new quá hạn
    if (Object.keys(promotions_new).length === 0) promotions_new[keys[0]] = production[keys[0]];
  });

  updates[`promotions-v2/promotions_new/`] = promotions_new;

  let promotionsOtherRef = firebase.database().ref('promotions-v2/promotions_other/');
  let promotions_other = {
    student: {},
    voucher: {}
  };

  await promotionsOtherRef.once('value', function(snapshot) {
    let student = snapshot.val().student;
    let studentKeys = Object.keys(student);
    studentKeys.map(key => {
      let item = student[key];
      if (item.data.endAt > currentDate) promotions_other.student[key] = item;
    });

    // thêm dữ liệu tối thiểu nếu tất cả khuyến mãi student quá hạn
    if (Object.keys(promotions_other.student).length === 0) promotions_other.student[studentKeys[0]] = student[studentKeys[0]];

    let voucher = snapshot.val().voucher;
    let voucherKeys = Object.keys(voucher);
    voucherKeys.map(key => {
      let item = voucher[key];
      if (item.endAt > currentDate) promotions_other.voucher[key] = item;
    });

    // thêm dữ liệu tối thiểu nếu tất cả khuyến mãi voucher quá hạn
    if (Object.keys(promotions_other.voucher).length === 0) promotions_other.voucher[voucherKeys[0]] = voucher[voucherKeys[0]];
  });

  updates[`promotions-v2/promotions_other/`] = promotions_other;
  // update firebase
  firebase
    .database()
    .ref()
    .update(updates);
}
