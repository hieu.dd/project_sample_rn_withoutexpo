var fs = require('fs');
var firebase = require('firebase');
var config = require('../../app/config');

var COMMANDS = {};

var LOGSTREAM = fs.createWriteStream('log.txt');
LOGSTREAM.once('open', function(fd) {
  main();
});

async function main() {
  initCommands();
  firebase.initializeApp(config.firebase);

  for (let func in COMMANDS) {
    let i = process.argv.findIndex(a => a === `--${func}`);
    if (i !== -1) {
      await COMMANDS[func]();
      break;
    }
  }

  LOGSTREAM.end();

  console.log('Done!!!');
  console.log('Press Ctrl+C to finish task');

  return false;
}

function initCommands() {
  registerCommand('writeData', writeData);
  registerCommand('config', configure);
  registerCommand('getEvent', getEvent);
  registerCommand('analyze', analyze);
}

function registerCommand(name, func) {
  COMMANDS[name] = func;
}

// FUNCTION COMMANDS START FROM HERE

async function writeData() {
  let updates = {};
  updates[`events/update_etag`] = require('./input.json');
  
  await firebase
    .database()
    .ref()
    .update(updates);
}

async function configure() {
  let subKey = 'saleRule';
  let firebaseConfig = require('./config.json');
  let updates = {};
  updates[`config/${subKey}`] = firebaseConfig[subKey];

  await firebase
    .database()
    .ref()
    .update(updates);
}

function analyze() {
  fs.readFile(`${__dirname}/analytic.json`, (err, p) => {
    if (!err) {
      let data = JSON.parse(p);
      for (let key in data) {
        let entry = data[key];
        if (entry.function_name === 'loadProducts' && entry.reason !== 'new json required') {
          console.log(entry.reason);
        }
      }
    }
  });
}

async function getEvent() {
  //await getData('events/update_etag', { child: { key: 'user_id', value: 'VVavl5taaFWGjvv4CZ0XbXJDznZ2' } });
  await getData('events/app_exception', { child: { key: 'function_name', value: 'syncFirebaseUserData' } });
}

async function getData(path, filter) {
  let childFilter = filter.child;
  let outFile = null;
  let snapshot = null;

  if (childFilter) {
    let { key, value } = childFilter;
    outFile = `${__dirname}/snapshot_${path.replace(/\//g, '_')}_${key}_${value}.json`;

    snapshot = await firebase
      .database()
      .ref(path)
      .orderByChild(key)
      .equalTo(value)
      .once('value');
  } else {
    outFile = `${__dirname}/snapshot_${path.replace(/\//g, '_')}.json`;
    snapshot = await firebase
      .database()
      .ref(path)
      .once('value');
  }

  if (snapshot && outFile) {
    let data = snapshot.val();
    data = data ? data : null;

    if (data) {
      await fs.writeFile(outFile, JSON.stringify(data, null, 2));
    } else {
      console.log('No data!');
    }
  }
}
