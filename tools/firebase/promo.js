var fs = require('fs');
var firebase = require('firebase');
var config = require('../../app/config');
var XLSX = require('xlsx');
var PRODUCTS = require('../../app/config/products.json');
var UPLOAD = false;

var COMMANDS = {};

var LOGSTREAM = fs.createWriteStream('log.txt');
LOGSTREAM.once('open', function(fd) {
  main();
});

async function main() {
  initCommands();
  firebase.initializeApp(config.firebase);

  for (let func in COMMANDS) {
    let i = process.argv.findIndex(a => a === `--${func}`);
    if (i !== -1) {
      await COMMANDS[func]();
      break;
    }
  }

  LOGSTREAM.end();

  console.log('Done!!!');
  console.log('Press Ctrl+C to finish task');

  return false;
}

function initCommands() {
  registerCommand('report', genReport);
  registerCommand('calibrate', calibrateQuantity);
  registerCommand('makePromo', makePromotions);
  registerCommand('makePromoOther', makePromoOther);
  registerCommand('makePromoNew', makePromoNew);
}

function registerCommand(name, func) {
  COMMANDS[name] = func;
}

// FUNCTION COMMANDS START FROM HERE
async function genReport() {
  let USE_PREDOWNLOAD_DATA = false;
  let report = require('./promo_templates/report.json');

  LOGSTREAM.write('START\n');
  let WB = XLSX.utils.book_new();

  for (let key in report) {
    let history = await firebase
      .database()
      .ref(`history/promotion_quantity/${key}`)
      .once('value');
    history = history.val();
    if (report[key].type === 'firebase') {
      for (let sku in history) {
        // count the undefined part
        let undefinedCounter = await firebase
          .database()
          .ref(`history/promotion_quantity/undefined/${sku}`)
          .once('value');
        undefinedCounter = undefinedCounter.val();
        let data = { ...history[sku], ...undefinedCounter };

        let buys = [];
        for (let buy in data) {
          if (data[buy].path.indexOf(report[key].path) !== -1) {
            let parts = buy.split('_');
            buys.push({
              before: parseInt(parts[0]),
              asia_id: parts[2],
              count: parseInt(data[buy].quantity),
              after: data[buy].value,
              ts: parts[1]
            });
          } else if (!undefinedCounter || !undefinedCounter[buy]) {
            LOGSTREAM.write(`mismatch promotion program, expect: ${report[key].path}, got: ${data[buy].path}\n`);
          }
        }

        buys.sort((a, b) => b.ts - a.ts);

        // validate if quantity subtract correctly
        if (buys.length > 0) {
          let remain = buys[0].after;
          let asia_id = [];
          for (let i = 0; i < buys.length; i++) {
            let b = buys[i];
            if (b.before - b.count !== b.after) {
              LOGSTREAM.write(`sku ${sku}: ${b.before}_${b.ts}_${b.asia_id} internal wrong: ${JSON.stringify(b)}\n`);
              LOGSTREAM.write(`DEBUG: ${JSON.stringify(buys)}\n`);
            }
            if (buys[i + 1] && b.before !== buys[i + 1].after) {
              LOGSTREAM.write(
                `sku ${sku}: ${b.before}_${b.ts}_${b.asia_id} interconnect wrong: current ${JSON.stringify(
                  b
                )}, next ${JSON.stringify(buys[i + 1])} (i=${i})\n`
              );
              LOGSTREAM.write(`DEBUG: ${JSON.stringify(buys)}\n`);
            }

            asia_id.push(b.asia_id);
          }

          // summarize the final count
          let p = PRODUCTS.find(e => parseInt(e.s) === parseInt(sku));
          report[key].reports.push({
            ma_vat_tu: sku,
            ten_sp: p ? p.n : report[key].name,
            da_ban: asia_id.length,
            con_lai: remain,
            danh_sach_chung_tu: asia_id.join(',')
          });
        }
      }

      // make worksheet report for this promo program
      let ws = XLSX.utils.json_to_sheet(report[key].reports, {
        skipHeader: false
      });
      XLSX.utils.book_append_sheet(WB, ws, report[key].name);
    }
  }

  await XLSX.writeFile(WB, 'reports.xlsx');
  await fs.writeFile('./report.json', JSON.stringify(report, null, 2));
}

async function calibrateQuantity() {
  await runDescreaseQuanityTransaction(
    'KM_KhaiTruong_DongThap__LAPTOP__TU_28_07_2018_DEN_31_08_2018__data_0',
    '1801021',
    'promotions-v2/promotions_new/KM_KhaiTruong_DongThap__LAPTOP__TU_28_07_2018_DEN_31_08_2018/data/KM_KhaiTruong_DongThap__LAPTOP__TU_28_07_2018_DEN_31_08_2018__data_0/quantity_left',
    -1,
    'calibrate'
  );
}

function runDescreaseQuanityTransaction(promotionKey, key, path, quantity, order_id) {
  let currentValue = null;

  return firebase
    .database()
    .ref(path)
    .transaction(
      current => {
        if (isNaN(parseInt(current))) {
          return;
        } else {
          currentValue = current;
          return current - quantity;
        }
      },
      current => {
        if (isNaN(parseInt(current))) {
          return;
        } else {
          currentValue = current;
          return current - quantity;
        }
      },
      (error, commited, value) => {
        if (!error && commited && currentValue !== null) {
          firebase
            .database()
            .ref(`history/promotion_quantity/${promotionKey}/${key}/${currentValue}_${Date.now()}_${order_id}`)
            .set({
              path,
              quantity,
              value: value.val()
            });
        }
      }
    );
}

async function makePromotions() {
  let INDEX_FROM = 28; // set it to null, to use key not index, or to max firebase index now + 1
  let TEMPLATE = require('./promo_templates/promotions.json');
  let workbook = XLSX.readFile('promotions.xlsx');
  let updates = {};

  for (let promotion_name in TEMPLATE) {
    let promotions = XLSX.utils.sheet_to_json(workbook.Sheets[promotion_name]);
    let p = TEMPLATE[promotion_name];
    let key = promotion_name;

    // use index as promotions key, to compat with old version
    if (INDEX_FROM !== null) {
      key = INDEX_FROM;
      INDEX_FROM += 1;
    }

    updates[`promotions-v2/promotions/${key}`] = {
      ...p,
      key: promotion_name,
      date: {
        startDate: new Date(p.date.startDate).getTime(),
        endDate: new Date(p.date.endDate).getTime()
      },
      data: promotions.reduce((acc, current) => {
        acc[current.sku] = {
          quantity: String(current.quantity),
          sku: String(current.sku)
        };
        if (current.price) {
          acc[current.sku].promotionPrice = String(current.price).replace(/,/g, '');
        }
        if (current.gifts) {
          acc[current.sku].promotionSkus = current.gifts.split(',');
        }
        if (current.note) {
          acc[current.sku].note = String(current.note);
        }
        if (p.limited_quantity) {
          acc[current.sku].limited_quantity = p.limited_quantity;
        }
        return acc;
      }, {}),
      limited_quantity: null
    };
  }

  if (UPLOAD) {
    await firebase
      .database()
      .ref()
      .update(updates);
  } else {
    await fs.writeFile(__dirname + '/promotions.json', JSON.stringify(updates, null, 2));
  }
}

async function makePromoOther() {
  let DATA = require('./promo_templates/promotions_other.json');
  let updates = {};

  for (let type in DATA) {
    if (type === 'disable') {
      continue;
    }

    for (let key in DATA[type]) {
      if (DATA.disable.indexOf(key) === -1) {
        let p = DATA[type][key];
        let time = {};
        let promo =
          type === 'student'
            ? {
                ...p,
                data: {
                  ...p.data,
                  startAt: new Date(p.data.startAt).getTime(),
                  endAt: new Date(p.data.endAt).getTime()
                }
              }
            : {
                ...p,
                startAt: new Date(p.startAt).getTime(),
                endAt: new Date(p.endAt).getTime()
              };

        updates[`promotions-v2/promotions_other/${type}/${key}`] = promo; // update to null to remove
      }
    }
  }

  if (UPLOAD) {
    await firebase
      .database()
      .ref()
      .update(updates);
  } else {
    await fs.writeFile(__dirname + '/promotions_other.json', JSON.stringify(updates, null, 2));
  }
}

async function makePromoNew() {
  let DATA = require('./promo_templates/promotions_new.json');
  let updates = {};

  for (let key in DATA) {
    updates[`promotions-v2/promotions_new/${key}`] = {
      ...DATA[key],
      dateTime: {
        ...DATA[key].dateTime,
        date: {
          start: new Date(DATA[key].dateTime.date.start).getTime(),
          end: new Date(DATA[key].dateTime.date.end).getTime()
        }
      }
    };
  }

  if (UPLOAD) {
    await firebase
      .database()
      .ref()
      .update(updates);
  } else {
    await fs.writeFile(__dirname + '/promotions_new.json', JSON.stringify(updates, null, 2));
  }
}
